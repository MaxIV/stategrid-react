import "preact/debug";
import { h, Component, render } from 'preact';
import { useState, useEffect } from 'preact/hooks';
import { signal, computed, batch } from '@preact/signals';

import { StateGrid } from "./grid.js";

function getPathFromURL() {
    const pathString = window.location.hash.slice(1);
    if (pathString) {
        return decodeURIComponent(pathString).split("/")
    }
    return [];
}

function putPathInURL(path) {
    let url = new URL(window.location);
    url.hash = "#" + path.join("/");
    window.history.pushState({}, "", url);
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

// Priority order of States, unless specified in the config
var defaultStatePrios = ["ON", "RUNNING", "OPEN", "EXTRACT",
                         "STANDBY", "OFF", "CLOSE", "INSERT", "MOVING", "DISABLE",
                         "INIT", "N/A", "ALARM", "FAULT", "UNKNOWN"];

// Main component
function App({config, deviceStates, gridStates, totalState}) {

    const [path, setPath] = useState(getPathFromURL());

    // Handle changing the path (e.g. the user clicks a cell that contains a grid)
    function setPathAndUrl(path) {
        putPathInURL(path);
        setPath(path);
    }

    // Handle browser back/forward
    window.addEventListener("popstate", () => setPath(getPathFromURL()));

    return h(StateGrid, {config, path, setPath: setPathAndUrl, deviceStates, gridStates, totalState});
}

// Build a mapping of all devices from the config, to signals.
// This is the "store" which we will update with new states as they arrive
function getDevices(config, states) {
    if (!config.grid) {
        return;
    }
    Object.entries(config.grid.cells).forEach(([cellId, cell]) => {
        if (cell.device) {
            // const newStateIndex = getRandomInt(0, defaultStatePrios.length);
            // const newState = defaultStatePrios[newStateIndex];
            states[cell.device] = signal({state: "N/A"});
        } else {
            getDevices(cell, states);
        }
    })
}


const TANGO_SUBSCRIBE = `
subscription Attributes($fullNames: [String]!) {
    attributes(fullNames: $fullNames) {
      device
      attribute
      value
      writeValue
      quality
    }
}
`;

const TANGO_READ_ATTRIBUTES = `
query ReadAttributes($attributes: [String]!) {
    attributes (fullNames: $attributes) {
        name
        value
    }
}`;

const TANGO_EXECUTE_COMMAND = `
mutation ExecuteCommand($device: String!, $command: String!, $argin: ScalarTypes) {
    executeCommand(device: $device, command: $command, argin: $argin) {
        ok
        message
        output
    }
}
`;


const throttle = (callback, time) => {
    //don't run the function if throttlePause is true
    if (throttlePause) return;
    //set throttlePause to true after the if condition. This allows the function to be run once
    throttlePause = true;
    //setTimeout runs the callback within the specified time
    setTimeout(() => {
        callback();
        //throttlePause is set to false once the function has been called, allowing the throttle function to loop
        throttlePause = false;
    }, time);
};


async function subscribe(wsurl, url, usdevice, deviceStates) {
    await fetch(url, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            query: TANGO_READ_ATTRIBUTES,
            variables: {
                attributes: [`${usdevice}/ChildData`]
            }
        })
    })
        .then(response => response.json())
        .then(data => {
            console.log("got", data)
            if (data.data.attributes) {
                const value = JSON.parse(data.data.attributes[0].value)
                for (let [name, info] of Object.entries(value.devices)) {
                    deviceStates[name].value = info;
                }
            } else {
                console.log("Error", data);
            }
        });
    const socket = new WebSocket(wsurl, "graphql-ws");
    socket.addEventListener("open", () => {
        console.log("ws open")
        console.log(socket)
        socket.addEventListener("message", m => {
            const message = JSON.parse(m.data);
            if (message.type === "data") {
                const data = message.payload.data;
                if (data.attributes.attribute == "versioncount") {
                    const version = data.attributes.value;
                    if (version === 0) {
                        return
                    }
                    fetch(url, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            query: TANGO_EXECUTE_COMMAND,
                            variables: {
                                device: usdevice,
                                command: "GetUpdates",
                                argin: [version - 1, version],
                            }
                        })
                    })
                        .then(response => response.json())
                        .then(data => {
                            console.log("got", data)
                            if (data.data.executeCommand.ok) {
                                const value = JSON.parse(data.data.executeCommand.output)
                                for (let [name, info] of Object.entries(value)) {
                                    deviceStates[name].value = info;
                                }
                            } else {
                                console.log("Error", data);
                            }
                        });
                }
            }
        });
        socket.send(JSON.stringify({"type": "connection_init", "payload":{}}))  // Needed?
        socket.send(JSON.stringify({
            id: "1",
            type: "start",
            payload: {
                query: TANGO_SUBSCRIBE,
                variables: {
                    fullNames: [`${usdevice}/VersionCount`]
                }
            }
        }));
    });
    return socket;
}


// Load the config and start the application
fetch(`/veritas_stategrid.json`)
    .then(response => {
        return response.json();
    })
    .then(config => {

        var deviceStates = {};
        getDevices(config, deviceStates);
        console.log("deviceStates", deviceStates);

        // Calculate grid states as derived from the device/grid states they
        // contain, recursively. These will also get automagically updated
        // when their states are updated.
        const gridStates = {};

        function derivedState(signals, prios) {
            console.log("signals", signals)
            return prios[Math.max(...signals.map(s => s && prios.indexOf(s.value.state)))];
        }

        function computeGridCells(grid, path) {
            const signals = Object.entries(grid.cells).map(([cellId, cell]) => {
                if (cell.grid) {
                    const result = computeGridCells(cell.grid, [...path, cellId]);
                    gridStates[[...path, cellId].join("/")] = result;
                    return result
                } else if (cell.device) {
                    return deviceStates[cell.device.toUpperCase()];
                }
            });
            const prios = grid.state_priorities || defaultStatePrios;
            return computed(() => ({state: derivedState(signals, prios)}))
        }
        const totalState = computeGridCells(config.grid, []);

        // const socket = subscribe("ws://localhost:9009/socket", "http://localhost:9009/db", config.state_device, deviceStates)
        const socket = subscribe("ws://taranta.maxiv.lu.se/veritas/socket", "http://taranta.maxiv.lu.se/veritas/db", config.state_device, deviceStates)

        // Fake a stream of random state changes
        // Batch update 1000 devices per second, as a performance test
        // const devices = Array.from(Object.keys(deviceStates));
        // setInterval(() => {
        //     batch(() => {
        //         for (let i = 0; i<=1000; i++) {
        //             const newStateIndex = getRandomInt(0, defaultStatePrios.length),
        //                   newState = defaultStatePrios[newStateIndex],
        //                   devices = Array.from(Object.keys(deviceStates)),
        //                   deviceIndex = getRandomInt(0, devices.length),
        //                   device = devices[deviceIndex];
        //             deviceStates[device].value = {state: newState};
        //         }
        //     });
        // }, 1000);

        render(h(App, { config, deviceStates, gridStates, totalState }), document.getElementById("container"))
    });
