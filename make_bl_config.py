import json
import re


def get_devices(dsconfig):
    for srv_name, srv in dsconfig["servers"].items():
        for inst_name, inst in srv.items():
            for cls_name, cls in inst.items():
                for dev_name, dev in cls.items():
                    yield dev_name.lower(), cls_name


DEVICE_PREFIX = r"{name}-(fe|o\d+|ea\d+)/.*"
DEVICE_SUBSYSTEM = r"[^/]+/([^/]+)/[^/]+"


def get_device_prefix(dev, blname):
    return re.match(DEVICE_PREFIX.format(name=blname), dev).group(1)


def get_device_subsystem(dev, blname):
    return re.match(DEVICE_SUBSYSTEM.format(name=blname), dev).group(1)


def build_beamline_stategrid_config(dsconfig, blname, state_device, title):
    # print(DEVICE_PREFIX.format(name=blname))
    devices = [
        (name, cls)
        for name, cls in get_devices(dsconfig)
        if re.match(DEVICE_PREFIX.format(name=blname), name)
    ]
    # print("\n".join(d for d, _  in devices))
    cols = {get_device_subsystem(d, blname) for d, _ in devices}
    rows = {get_device_prefix(d, blname) for d, _ in devices}
    return {
        "title": title,
        "state_device": state_device,
        "state_priorities": ["N/A", "RUNNING", "ON", "OPEN", "EXTRACT",
                             "STANDBY", "OFF", "CLOSE", "INSERT", "MOVING",
                             "DISABLE", "INIT", "ALARM", "FAULT", "UNKNOWN"],
        "grid": {
            "column_names": list(cols),
            "row_names": list(rows),
            "cells": {
                f"{get_device_subsystem(dev, blname)},{get_device_prefix(dev, blname)}": {
                    "device": dev
                }
                for dev, cls in devices
            }
        }
    }


if __name__ == "__main__":
    import sys

    with open(sys.argv[1]) as f:
        dsconfig = json.load(f)
        print(json.dumps(build_beamline_stategrid_config(dsconfig, "b316a", "b316a/ctl/us-01", "Veritas"), indent=4))
