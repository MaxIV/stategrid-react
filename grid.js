import { h, Component, render } from 'https://esm.sh/preact';

// Get the grid at a given "path" in the configuration
function getGrid(config, path) {
    for (var step of path) {
        config = config.grid.cells[step];
    }
    return config.grid;
}

function stripHTMLTags(s) {
    return s.replace(/<\/?[^>]+(>|$)/g, " ");
}

// Main component
export function StateGrid({ config, path, setPath, deviceStates, gridStates, totalState }) {
    return h("div", {className: "stategrid"}, [
        h(Path, {title: config.title, path, setPath, gridStates, totalState}),
        h(Grid, {grid: getGrid(config, path), path, setPath, deviceStates, gridStates}),
    ]);
}

// Component that shows location of the current grid in the config
function Path({ title, path, setPath, gridStates, totalState }) {
    var subPaths = [];
    for (let i = 0; i < path.length; i++) {
        subPaths.push(path.slice(0, i+1))
    }
    return h("div", {className: `path`}, [
        h("span", {
            className: `title state-${totalState.value.state}`,
            onClick: () => setPath([])
        }, title),
        ...subPaths.map(p => h("span", {
            className: `path-step state-${gridStates[path.join("/")]}`,
            onClick: () => setPath(p)
        }, `${stripHTMLTags(p[p.length - 1])}` ))
    ]);
}

// Component representing a single cell
function Cell({cell, cellId, path, setPath, deviceStates, gridStates}) {
    if (cell === undefined || cell.grid && Object.keys(cell.grid.cells).length === 0) {
        // Nothing to see here...
        return h("td", {className: "empty"}, "X")
    } else if (cell.device) {
        const state = deviceStates[cell.device].value.state;
        return h("td", {className: `device state-${state}`}, h("div", {}, state));
    } else if (cell.grid) {
        const cellPath = [...path, cellId];
        const key = cellPath.join("/"),
              state = gridStates[key].value.state;
        return h("td", {className: `grid state-${state}`, onClick: () => setPath(cellPath)}, h("div", {}, state))
    } else {
        // I don't think this should evern happen...
        console.warning("???", cell)
    }
}


// Component that displays a given grid
function Grid({ grid, path, setPath, deviceStates, gridStates }) {
    return h("table", {className: "grid"}, [
        h("thead", {}, [
            h("tr", {}, [
                h("th", {className: "filler"}),
                ...grid.column_names.map(col => h("th", {className: col? "column" : "spacer", dangerouslySetInnerHTML: {__html: col}})),
                h("th", {className: "filler"}),
            ])
        ]),
        h("tbody", {},
            grid.row_names.map(row => {
              return h("tr", {}, [
                  h("th", {className: (row? "row" : "spacer") + " left"}, row),
                  ...grid.column_names.map(col => {
                      if (row && col) {
                          const cellId = `${col},${row}`,
                                cell = grid.cells[cellId];
                          return Cell({cell, cellId, path, setPath, deviceStates, gridStates})
                      } else {
                          return h("td", {className: "spacer"});
                      }
                  }),
                  h("th", {className: (row? "row" : "spacer") + " right"}, row),
              ])
          })
        )
    ])
}
