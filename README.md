# Stategrid (P)react port

Prototype web based Stategrid.

Currently just renders a Stategrid configuration and allows navigation, and properly calculates states. However there is no communication with a control system yet.

A few minor features are still missing, e.g. tooltips and errors.

To try it, simply serve the static files by e.g. running this command in the repo dir:

    python -m http.server 
    
and then visit http://localhost:8000 in your web browser.

Based on Preact which is a React API clone, for convenience. Porting to React should just be a matter of changing the imports.
